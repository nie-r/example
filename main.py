# suma simple
def f_suma(param_a, param_b):
    print(int(param_a) + int(param_b))

# resta simple
def f_resta(param_a, param_b):
    print(int(param_a) - int(param_b))

print('Calculando la suma:')
f_suma('5',5)
f_suma('50',50)

print('Calculando la resta:')
f_resta('5',5)
f_resta('50',55)
